export const wishListInputFieldIsValid = (inputValue) =>{
    return isNotEmpty(inputValue) ;
}

const isNotEmpty = (inputValue) => {
    return inputValue.trim() !== ''
}

const isCorrectBrand = (inputValue) => {
    return inputValue.trim() === 'a7c'
}