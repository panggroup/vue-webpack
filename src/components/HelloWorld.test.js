import { shallowMount } from '@vue/test-utils'
import HelloWorld from './HelloWorld.vue'

describe("HelloWorld.vue" , () =>{
    describe("Render" , () => {
        const wrapper = shallowMount(HelloWorld, {
            propsData: {
              msg: 'ABC'
            }
        })
        
        it("render correctly" , () =>{
            expect(wrapper.element).toMatchSnapshot() //go to snapshot folder
        })
        // it("has div with hello class" , () =>{
        //     expect(wrapper.find('.hello').exists()).toBe(true)
        // })
        // it("show input message under h1" , () =>{
        //     expect(wrapper.find('div.hello > h1').exists()).toBe(true)
        // })
        it("should show msg ABC" , () =>{
            expect(wrapper.find('.hello').text()).toBe("ABC")
        })
        it("should change msg when send new prop data" , () =>{
            const wrapper = shallowMount(HelloWorld, {
                propsData: {
                  msg: 'DEF'
                }
            })
            expect(wrapper.find('.hello').text()).toBe("DEF")
        })  
    })
})