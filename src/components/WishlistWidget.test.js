import {shallowMount} from '@vue/test-utils'
import WishlistWidget from './WishlistWidget.vue'
import Wishlist from './Wishlist.vue'

describe('WishlistWidget.vue' , () => {
    let wrapper
    let textbox
    let button
    let wishlist

    beforeEach( ()=>{
        wrapper = shallowMount(WishlistWidget)
        textbox = wrapper.find('input')
        button = wrapper.find('button')
        wishlist = wrapper.findComponent(Wishlist)        
    })

    describe('Render' , ()=>{
        it('render correctly' , () =>{
            expect(wrapper.element).toMatchSnapshot()
        })
    })
    describe('Add item to list' , ()=>{
        describe('scenario 1 -> empty input' , ()=>{
            it('should not add item to list' , () =>{
                textbox.setValue('')
                button.trigger('click')
                expect(wishlist.props('items')).toEqual([])
            })
            it('should not add whitespace item to list' , () =>{
                textbox.setValue('    ')
                button.trigger('click')
                expect(wishlist.props('items')).toEqual([])
            })
        }) //Remove log empty and use invalid input using mock
        describe('scenario 2 -> valid input' , ()=>{
            beforeEach(() =>{
                textbox.setValue('a7c')
                button.trigger('click')
            })
            it('should add item to list' , () =>{
                expect(wishlist.props('items')).toEqual(['a7c'])
            })
            it('should add item in order' , () => {
                textbox.setValue('a7cII')
                button.trigger('click')
                expect(wishlist.props('items')).toEqual(['a7c' , 'a7cII'])
            })
            it('should clear data in textbox' , () =>{
                expect(textbox.text()).toBe('')
            })
        })
    })
})